Source: parfive
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               help2man <!nodoc>,
               graphviz <!nodoc>,
               python3-aiofiles,
               python3-aioftp,
               python3-aiohttp,
               python3-all,
               python3-setuptools,
               python3-setuptools-scm,
               python3-sphinx <!nodoc>,
               python3-sphinx-astropy <!nodoc>,
               python3-sphinx-autodoc-typehints <!nodoc>,
               python3-tqdm
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/parfive
Vcs-Git: https://salsa.debian.org/python-team/packages/parfive.git
Homepage: https://parfive.readthedocs.io/
Rules-Requires-Root: no

Package: python3-parfive
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Recommends: ${python3:Recommends}
Suggests: ${python3:Suggests}
Description: HTTP and FTP parallel file downloader for Python
 Parfive is a small library for downloading files, its objective is to
 provide a simple API for queuing files for download and then
 providing excellent feedback to the user about the in progress
 downloads. It also aims to provide a clear interface for inspecting
 any failed downloads.
 .
 Parfive supports downloading files over either HTTP or FTP using
 aiohttp and aioftp.

Package: python-parfive-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: documentation for the parfive Python library
 Parfive is a small library for downloading files, its objective is to
 provide a simple API for queuing files for download and then
 providing excellent feedback to the user about the in progress
 downloads. It also aims to provide a clear interface for inspecting
 any failed downloads.
 .
 Parfive supports downloading files over either HTTP or FTP using
 aiohttp and aioftp.
 .
 This package provides documentation for parfive

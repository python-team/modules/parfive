tqdm>=4.27.0
aiohttp

[docs]
sphinx
sphinx-automodapi
sphinx-autodoc-typehints
sphinx-contributors
sphinx-book-theme

[ftp]
aioftp>=0.17.1

[tests]
pytest
pytest-localserver
pytest-asyncio
pytest-socket
pytest-cov
aiofiles
